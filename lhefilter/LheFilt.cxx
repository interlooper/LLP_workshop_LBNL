
#include "TLorentzVector.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include "LheFilt.h"
#include <functional>
#include <map>
#include <stdio.h>
#include <stdlib.h>

#include "boost/program_options/cmdline.hpp"
#include "boost/program_options/options_description.hpp"
#include "boost/program_options/parsers.hpp"
#include "boost/program_options/variables_map.hpp"
#include "boost/foreach.hpp"
#include "boost/tokenizer.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::stringstream;
using std::scientific;
using std::uppercase;
using std::setw;
using std::setfill;

// ==========================================================================================================================================

// pt sorted vector map
typedef std::map< float, TLorentzVector, std::greater<float> > PtMap;


LheFilt::LheFilt(): 
   m_outputFileIdx(0),
   m_nevents_split(60000),
   m_header_inited(0),
   debug(0),
   m_eff(1.) {


   m_fileList.clear();
   m_argStrEvent.clear();

   m_xsecs.clear();
   m_errxsecs.clear();

   m_nevread = 0;
   m_nevkept = 0;
}


// ==========================================================================================================================================

int main(int argc, char**argv) {

  namespace po = boost::program_options;


   // input parameters
   po::options_description desc("Options");
   desc.add_options()
      ("help", "write help msg")
      ("i",   po::value<std::string>(),  "input files")
      ("output_name,o",   po::value<std::string>(),  "output file name pattern output_name.XXXX.events")
      ("filter,f", po::value< vector<string> >()->composing(), "filter")
      ("nevents,n", po::value< int>(), "number of events to split")
      ;

   po::variables_map vm;
   po::store(po::parse_command_line(argc, argv, desc), vm);
   po::notify(vm);


   // resolve input file
   string inputfile = "inputFiles.txt"; // default
   if(!vm.count("i")) {
      cout << "No input file specified, using default: " <<  inputfile<< endl;
   } else {
      inputfile = vm["i"].as<string>();
   }

   // outputname pattern
   string name_pattern = "LheFilt.pwgevents";
   if(!vm.count("output_name")) {
      cout << "No output name_pattern specified, using default: " <<  name_pattern << endl;
   } else {
      name_pattern = vm["output_name"].as<string>();
   }


   // example how to read stuff
   if( vm.count("help") ) {
      std::cout<<desc<<'\n';
      return 1;
   }



   LheFilt* lhefilt = new LheFilt;
   lhefilt->debug = 0;
   lhefilt-> Set_Input(inputfile);
   lhefilt-> Set_OutputPattern(name_pattern);
   if(vm.count("filter"))lhefilt-> Set_Filters(vm["filter"].as< vector<string> >());
   if(vm.count("nevents"))lhefilt-> Set_NeventSplit( vm["nevents"].as<int>());

   lhefilt->Print_Configuration();
   lhefilt->FindInputFiles();

   lhefilt->ConstructLheHeader();
   lhefilt->FileLoop();
}

// ==========================================================================================================================================
void LheFilt::Print_Configuration() {

   cout << "LheFilt configuration \n"
      << "m_nevents_split = " << m_nevents_split << "\n"
      << endl;

 
}



void LheFilt::Set_Filters(std::vector<std::string>  filters) {


   for(size_t i=0; i< filters.size(); ++i){

      string filter = filters[i];

      // associate filters with strings here
      if(filter=="filterWW_pt80") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_WW_pt80); } 
      else if(filter=="filterWW_pt40") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_WW_pt40); } 
      else if(filter=="filter_split") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_Splitter); } 
      else if(filter=="filterZZ_pt13pt5") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_ZZ_pt13pt5); } 
      else if(filter=="filterZZ_pt5pt5") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_ZZ_pt5pt5); } 
      else if(filter=="filterZZ_2mZ_pt15") { cout << "Enabeling filter "<< filter<< endl; m_filters.push_back(&LheFilt::FilterEvent_ZZ_2mZ_pt15); } 
      else 
      {
         cout << "Cannot resolved filter: " << filter << endl;
         exit(1);
      }

   }
}

// ==========================================================================================================================================

void LheFilt::FindInputFiles () {

   // open the file "inputFiles.txt" containing the list of input file names

   std::string   argStr;
   std::ifstream inputFiles(m_inputFileName.c_str());

   while (!inputFiles.eof()) {
      std::getline(inputFiles,argStr);
      if (argStr.length()<2)            break;
      if (argStr.substr(0,1) == "#")    continue;
      for (size_t i=0,n; i <= argStr.length(); i=n+1) {
         n = argStr.find_first_of(',',i);                       // input files may come as a comma-separated list
         if (n == std::string::npos) n = argStr.length();
         std::string tmp = argStr.substr(i,n-i);
         m_fileList.push_back(tmp);
      }
   }

   for (UInt_t iFile=0; iFile<m_fileList.size(); iFile++) {
      std::cout << " FindInputFiles() :  file = " << m_fileList[iFile].c_str() << std::endl;
   }

   cout << " FindInputFiles() :  nFiles = " << m_fileList.size() << endl;

   return;
}

// ==========================================================================================================================================

void LheFilt::ConstructLheHeader() {

   cout << " LheFilt::ConstructLheHeader():: Fast loop over all events to get sample cross section, calculate the <xsection> and construct lhe header" << endl;


   stringstream header_end;
   string lineXsection;

   int n_selected =0;
   for (UInt_t iFile=0; iFile<m_fileList.size(); iFile++) {
      m_inputFile.open(m_fileList[iFile].c_str());
      if (!m_inputFile) {
         cout << " ConstructLheHeader():  Failed to open input file " << m_fileList[iFile] << endl;
         break;
      } else {
         cout << " ConstructLheHeader():  Opened input file " << m_fileList[iFile] << endl;
      }

      std::string  argStr;

      m_xsec    = 0.;
      m_errxsec = 0.;

      for (UInt_t line=0; line>=0; line++) {
         std::getline(m_inputFile,argStr);
         if (iFile == 0) m_header << argStr << endl;
         if (argStr.substr(0,6) == "<init>") {
           if (debug >= 1) cout << " ReadLheHeader():  Found <init> " << endl;
           break;
         }
      }

      std::getline(m_inputFile,argStr);
      if (iFile == 0) m_header<< argStr << endl;

      // reading cross section line, don't dump it, only extract xsection
      std::getline(m_inputFile,argStr);

      //  if (iFile == 0) m_outputFile << argStr << endl;
      if (debug >= 9)
        cout << "Processing xsec line: " << argStr << endl;
      std::istringstream ixsec(argStr.substr(0,13));
      std::istringstream ierrxsec(argStr.substr(13,13));
      ixsec    >> m_xsec;
      ierrxsec >> m_errxsec;
      m_xsecs.push_back(m_xsec);
      m_errxsecs.push_back(m_errxsec);
      if (debug >= 1)
        cout << "xsec = " << m_xsec << " +/- " << m_errxsec << endl;


      // if last file - calculate xsection and complete the header
      if(iFile == 0) {
         lineXsection = argStr;
      }

      std::getline(m_inputFile,argStr);
      if (iFile == 0) header_end << argStr << endl;
      if (argStr.substr(0,7) == "</init>") {
        if(debug >= 1)
          cout << " ReadLheHeader():  Found end of <init> " << endl;
        
        while (ReadLheEvent()) {
          bool pass = ExecuteFilters();
          if(pass) n_selected++;
          if (debug >= 1) cout << "N read/selected : " << n_selected << " / " << m_nevread << endl;
        }

      }
      m_inputFile.close();

   }

   m_eff = n_selected/(double)m_nevread;
   cout << "ConstructLheHeader():  Found end of files,  m_nevread = " << std::setw(9) << m_nevread
                                   << "    n_selected = " << std::setw(9) << n_selected << endl;
   cout << "FilterEfficiency = " << std::setw(9) << m_eff << endl;
  

   // assign new cross section * efficiency
   boost::char_separator<char> sep(" \f\t\v");
   typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;

   Tokenizer tok(lineXsection, sep);
   std::vector< std::string > tokens(tok.begin(), tok.end());

   stringstream number;
   number.precision(5);

   // set m_xsec,ierrxsec
   ProcessXsecs();
   number << "  " << uppercase << scientific<<  m_xsec << "  " <<  m_errxsec;
   m_header << number.str() << "  " <<  tokens[2] << "  " << tokens[3] <<endl;
   m_header << header_end.str();

   // reset counters
   m_nevread = 0;
   m_nevkept = 0;

   cout << "ConstructLheHeader() finished" << endl;

   return;

}

void LheFilt::FinalizeFile(bool opennewfile){

   m_outputFile << m_tail.str();
//    m_outputFile << m_argStrEvent.at(0) << endl;
//    m_outputFile << m_argStrEvent.at(1) << endl;
   m_outputFile.close();

   m_outputFileIdx++;

   if(opennewfile) OpenOutputFile();
}

// ==========================================================================================================================================

void LheFilt::OpenOutputFile( ){

   stringstream fn;
   fn << setw( 5 ) << setfill( '0' ) << m_outputFileIdx+1;
//    std::string outputFileName("LheFilt.pwgevents."+fn.str()+".lhe");
   std::string outputFileName(m_outputNamePattern+"._"+fn.str()+".events");

   m_currentOutputFileName = outputFileName; // export

   m_outputFile.open(outputFileName.c_str());
   if (!m_outputFile) {
      cout << " FileLoop():  Failed to open output file " << outputFileName << endl;
   } else {
      cout << " FileLoop():  Opened output file " << outputFileName << endl;
   }

   m_outputFile << m_header.str();

}

// ==========================================================================================================================================
void LheFilt::FileLoop() {


   OpenOutputFile();



   int m_nevtFile = 0;
   for (UInt_t iFile=0; iFile<m_fileList.size(); iFile++) {
      m_inputFile.open(m_fileList[iFile].c_str());
      if (!m_inputFile) {
         cout << " FileLoop():  Failed to open input file " << m_fileList[iFile] << endl;
         break;
      } else {
         cout << " FileLoop():  Opened input file " << m_fileList[iFile] << endl;
      }

      ReadLheHeader(iFile);

      while (ReadLheEvent()) {
         bool pass = ExecuteFilters();
         if(pass){

            ++m_nevtFile;
            if(m_nevents_split && m_nevtFile > m_nevents_split){ m_nevtFile=1; FinalizeFile(1); }

            WriteEvent();
         }

      }

      cout << " FileLoop():  Found end of file,  m_nevread = " << std::setw(9) << m_nevread << " ,  m_nevkept = " << std::setw(9) << m_nevkept << endl;
      m_inputFile.close();
   }

//    ProcessXsecs();

      if(  fabs( m_nevkept/(double) m_nevread/m_eff -1) > 1e-6) {
         cout << "ERROR, different efficiency of dumped events and used in cross section after filter! " 
            << m_nevkept/(double) m_nevread << " " << m_eff << endl;
      }


      FinalizeFile(0);
      // remove the last file if statistics not met -> keep them (Simone, 30/07/2013)
      if(m_nevtFile < m_nevents_split) { 
         cout << "WARNING: Not enough events in last file, but we'll keep it" << endl;
	 /*
         int ret = system( ("rm -fv " + m_currentOutputFileName).c_str() );
         if (ret != 0) {
           std::cerr << "Problem in deleting old result: " << m_currentOutputFileName << std::endl;
           return;
         }
	 */
      }


      cout << "FilterEfficiency = " << std::setw(9) << m_eff << endl;

   return;
}

/*
// ==========================================================================================================================================

void LheFilt::ReadLheHeader(UInt_t iFile) {

   std::string  argStr;

   m_xsec    = 0.;
   m_errxsec = 0.;

   for (UInt_t line=0; line>=0; line++) {
      std::getline(m_inputFile,argStr);
      if (iFile == 0) m_outputFile << argStr << endl;
      if (argStr.substr(0,6) == "<init>") {
         cout << " ReadLheHeader():  Found <init> " << endl;
         break;
      }
   }

   std::getline(m_inputFile,argStr);
   if (iFile == 0) m_outputFile << argStr << endl;

   std::getline(m_inputFile,argStr);
   if (iFile == 0) m_outputFile << argStr << endl;
   std::istringstream ixsec(argStr.substr(0,13));
   std::istringstream ierrxsec(argStr.substr(13,13));
   ixsec    >> m_xsec;
   ierrxsec >> m_errxsec;
   cout << " ReadLheHeader():  Cross section = " << m_xsec << " +- " << m_errxsec << " pb" << endl;
   m_xsecs.push_back(m_xsec);
   m_errxsecs.push_back(m_errxsec);

   std::getline(m_inputFile,argStr);
   if (iFile == 0) m_outputFile << argStr << endl;
   if (argStr.substr(0,7) == "</init>") {
      cout << " ReadLheHeader():  Found end of <init> " << endl;
   }

   return;
}
// ==========================================================================================================================================

*/
void LheFilt::ReadLheHeader(UInt_t iFile) {

   std::string  argStr;

   m_xsec    = 0.;
   m_errxsec = 0.;

   for (UInt_t line=0; line>=0; line++) {
      std::getline(m_inputFile,argStr);
      if (argStr.substr(0,7) == "</init>") {
//          cout << " ReadLheHeader():  Found end of <init> " << endl;
         break;
      }
   }


   return;
}

// ==========================================================================================================================================

bool LheFilt::ReadLheEvent() {

   if(debug >= 1) cout << "ReadLheEvent - entering" << endl;

   m_nlep      = 0;
   m_nZ        = 0;
   m_npar      = 0;
   m_xwgtup    = 0.;
   m_pdg9      = 0;
   m_pdglep.clear();
   m_fourMomZ.clear();
   m_fourMomWp.clear();
   m_fourMomWm.clear();
   m_fourMomLep.clear();
   (m_fourMom9).SetXYZT(0.,0.,0.,0.);

   m_argStrEvent.clear();

   std::string  argStr;

   //skip invalid lines
   std::getline(m_inputFile,argStr);
   bool endOfFile=false;
   while (argStr == "" && !endOfFile) {
     if (m_inputFile.fail()) endOfFile=true;
     std::getline(m_inputFile,argStr);
   }
   if (endOfFile) {
     if (debug >= 1) cout << "ReadLheEvent - exiting, END OF FILE" << endl;
     return false;
   }

   if (argStr.substr(0,19) == "</LesHouchesEvents>") {
      if(!m_header_inited) {
         m_header_inited = 1;
         m_tail << argStr << endl;
         std::getline(m_inputFile,argStr);
         m_tail << argStr << endl;
      }
//       m_argStrEvent.push_back(argStr);
      return false;
   }
   m_argStrEvent.push_back(argStr);
   if (argStr.substr(0,7) == "<event>") {
//           cout << " ReadLheEvent():  Found <event> " << endl;
      m_nevread++;
   }

   std::getline(m_inputFile,argStr);
   m_argStrEvent.push_back(argStr);
   std::istringstream inpar(argStr.substr(0,7));
   std::istringstream ixwgtup(argStr.substr(14,13));
   inpar   >> m_npar;
   ixwgtup >> m_xwgtup;
   if(debug >= 2) cout << " npar = " << m_npar << ", xwgtup = " << m_xwgtup << endl;

   Int_t    pdg(0);
   Double_t px(0.), py(0.), pz(0.), e(0.);

   for (Int_t j=0; j<m_npar; j++) {
      std::getline(m_inputFile,argStr);
      m_argStrEvent.push_back(argStr);
      std::istringstream ipdg(argStr.substr(0,8));
      std::istringstream  ipx(argStr.substr(38,17));
      std::istringstream  ipy(argStr.substr(38+17,17));
      std::istringstream  ipz(argStr.substr(38+2*17,17));
      std::istringstream   ie(argStr.substr(38+3*17,17));
      ipdg >> pdg;
      ipx  >> px;
      ipy  >> py;
      ipz  >> pz;
      ie   >> e;
      if(debug >= 20) cout << " pdg = " << pdg << " , px = " << px <<  " , py = " << py << " , pz = " << pz << " , e = " << e << endl;
      if (pdg == 23) {
         m_fourMomZ.push_back(TLorentzVector(px,py,pz,e) );
         m_nZ++;
      }
      if (pdg ==  24) { m_fourMomWp.push_back(TLorentzVector(px,py,pz,e) ); }
      if (pdg == -24) { m_fourMomWm.push_back(TLorentzVector(px,py,pz,e) ); }

      Int_t lep(-1);
      if (pdg == -11) lep = 0;
      if (pdg == +11) lep = 1;
      if (pdg == -13) lep = 2;
      if (pdg == +13) lep = 3;
      if (pdg == -15) lep = 4;
      if (pdg == +15) lep = 5;
      if (lep >= 0) {
         m_pdglep.push_back(pdg);
         m_fourMomLep.push_back(TLorentzVector(px,py,pz,e) );
         m_nlep++;
      }
      if (j+1 == 9) {
         m_pdg9 = pdg;
         m_fourMom9.SetXYZT(px,py,pz,e);
      }
   }

   if(debug >= 10) cout << "multiplicities: Wp Wn Z lep : " << m_fourMomWp.size() << " " << m_fourMomWm.size() << " " << m_fourMomZ.size() << " " << m_fourMomLep.size() << endl;
   //   if(debug >= 20) cout << " pdg9 = " << m_pdg9 << ", pdglep = " << m_pdglep[3] << ", nZ = " << m_nZ << endl;

   bool endOfEvent=false;
   while (!endOfEvent) {
     std::getline(m_inputFile,argStr);
     if (argStr.substr(0,8) == "</event>") {
       if(debug >= 1)  cout << " found end of event" << std::flush << endl;
       m_argStrEvent.push_back(argStr);
       endOfEvent=true;
     } else {
       //Still some extra info to be saved
       if (debug >= 10) cout << "Extra info: " << argStr << std::flush << endl;
       m_argStrEvent.push_back(argStr);
       //if (argStr.substr(0,4) == "#pdf") {
       //  //    cout << " found #pdf" << endl;
       //  std::getline(m_inputFile,argStr);
       //  m_argStrEvent.push_back(argStr);
       //}
       
     }
   } //loop over file

   if(debug) cout << "ReadLheEvent - exiting" << endl;

   return true;
}

// ==========================================================================================================================================

void LheFilt::ProcessXsecs() {

   cout << " ProcessXsecs():  No of inputs = " << m_xsecs.size() << endl;

   m_xsec    = 0.;
   m_errxsec = 0.;

   double sum1(0.), sum2(0.);

   for (UInt_t j=0; j<m_xsecs.size(); j++) {
      cout << " ProcessXsecs():  file " << std::setw(3) << j << "  cross section (pb) = " << m_xsecs.at(j) << " +- " << m_errxsecs.at(j) << endl;
      sum1 += 1. / pow(m_errxsecs.at(j),2);
      sum2 += m_xsecs.at(j) / pow(m_errxsecs.at(j),2);
   }

   m_xsec    = sum2 / sum1;
   m_errxsec = sqrt(1./sum1);

   cout << " ProcessXsecs():   combined cross section (pb) = " << m_xsec << " +- " << m_errxsec << endl;

   m_xsec    *= m_eff;
   m_errxsec *= m_eff;

   cout << " ProcessXsecs():   filtered cross section (pb) = " << m_xsec << " +- " << m_errxsec << " <-- using " << endl;

   return;
}

// ==========================================================================================================================================

void LheFilt::WriteEvent() {

   for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
      m_outputFile << m_argStrEvent.at(j) << endl;
   }

   m_nevkept++;

   return;
}

// ==========================================================================================================================================

bool LheFilt::ExecuteFilters() {

   if(debug) cout << "ExecuteFilters - entering" << endl;

   bool pass = true;

   for(size_t i=0; i< m_filters.size(); ++i){


      testFunction = m_filters.at(i);

       if(! (this->*testFunction)()){  pass = false; break; }
       

   }

   return pass;
}

// ==========================================================================================================================================
// definitions of filters below
// ==========================================================================================================================================
void LheFilt::FilterEvent() {

   TLorentzVector fourMom4l;

   fourMom4l = m_fourMomLep[0] + m_fourMomLep[1] + m_fourMomLep[2] + m_fourMomLep[3];

   Bool_t keepEvent(false);
   //      for (uint j=0; j<4; j++) {
   //    if (m_fourMomLep[j].Pt() > 7.5 && fabs(m_fourMomLep[j].PseudoRapidity()) < 2.8) keepEvent = true;
   //      }
   if (fourMom4l.M() > 40.)  keepEvent = true;

   if (!keepEvent)           return;

   for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
      m_outputFile << m_argStrEvent.at(j) << endl;
   }

   m_nevkept++;

   return;
}


// ==========================================================================================================================================

void LheFilt::FilterEvent_WZ() {


   PtMap leptons;
   leptons[m_fourMomLep[0].Pt()];
   leptons[m_fourMomLep[1].Pt()];
   leptons[m_fourMomLep[2].Pt()];



   PtMap::iterator it = leptons.begin();
   TLorentzVector Mll = it-> second; 
   it++;
   Mll+= it-> second;

   Bool_t keepEvent(false);
   //      for (uint j=0; j<4; j++) {
   //    if (m_fourMomLep[j].Pt() > 7.5 && fabs(m_fourMomLep[j].PseudoRapidity()) < 2.8) keepEvent = true;
   //      }
   if (Mll.M() > 40.)  keepEvent = true;

   if (!keepEvent)           return;

   for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
      m_outputFile << m_argStrEvent.at(j) << endl;
   }

   m_nevkept++;

   return;
}

// ==========================================================================================================================================

bool LheFilt::FilterEvent_ZZ_pt13pt5() {

   if(m_fourMomLep.size()!=2 && m_fourMomLep.size()!=4){
      cout << "This is not ZZ case, number of letpons is " << m_fourMomLep.size() << endl;

      if(m_fourMomLep.size()==0) {
         cout << "This is a strange event with no leptons. Should not happen often" << endl;

         for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
            cout << m_argStrEvent.at(j) << endl;
         }

         return 0;

      } else exit(1);
   }


   // filter on eta
   PtMap leptons;
   BOOST_FOREACH(TLorentzVector &l, m_fourMomLep) {

      if( fabs(l.Eta()) < 2.7 ) leptons[l.Pt()]=l;

   }


   // multiplicity cut
   if(leptons.size()<2) return 0;


   // pt cuts
   PtMap::iterator firstLep = leptons.begin();
   PtMap::iterator secondLep = leptons.begin(); secondLep++;

   bool pass = ( firstLep-> second.Pt() > 13) && (secondLep-> second. Pt() > 5); // in MEV

    return pass;
}

// ==========================================================================================================================================

bool LheFilt::FilterEvent_ZZ_pt5pt5() {

   if(m_fourMomLep.size()!=2 && m_fourMomLep.size()!=4){
      cout << "This is not ZZ case, number of letpons is " << m_fourMomLep.size() << endl;

      if(m_fourMomLep.size()==0) {
         cout << "This is a strange event with no leptons. Should not happen often" << endl;

         for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
            cout << m_argStrEvent.at(j) << endl;
         }

         return 0;
      } else exit(1);
   }


   // filter on eta
   PtMap leptons;
   BOOST_FOREACH(TLorentzVector &l, m_fourMomLep) {

      if( fabs(l.Eta()) < 2.7 ) leptons[l.Pt()]=l;

   }


   // multiplicity cut
   if(leptons.size()<2) return 0;


   // pt cuts
   PtMap::iterator firstLep = leptons.begin();
   PtMap::iterator secondLep = leptons.begin(); secondLep++;

   bool pass = ( firstLep-> second.Pt() > 5) && (secondLep-> second. Pt() > 5); // in MEV

    return pass;
}

// ==========================================================================================================================================

bool LheFilt::FilterEvent_WW_pt80() {
   /* 
    * Apply high pt filter on WW samples with anomalous couplings
    * - at least two leptons within |eta|<2.7
    * - pt1 > 80 GeV
    */


   if(m_fourMomLep.size()!=2 ){
      cout << "This is not WW case, number of letpons is " << m_fourMomLep.size() << endl;

      if(m_fourMomLep.size()==0) {
         cout << "This is a strange event with no leptons. Should not happen often" << endl;
         for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
            cout << m_argStrEvent.at(j) << endl;
         }

         return 0;
      } else exit(1);
   }


   // filter on eta
   PtMap leptons;
   BOOST_FOREACH(TLorentzVector &l, m_fourMomLep) {

      if( fabs(l.Eta()) < 2.7 ) leptons[l.Pt()]=l;

   }


   // multiplicity cut
   if(leptons.size()<2) return 0;


   // pt cuts
   PtMap::iterator firstLep = leptons.begin();
   PtMap::iterator secondLep = leptons.begin(); secondLep++;


   // leading pt
   bool pass = ( firstLep-> second.Pt() > 80); // in GEV

   return pass;

}
// ==========================================================================================================================================

bool LheFilt::FilterEvent_WW_pt40() {
   /* 
    * Apply high pt filter on WW samples with anomalous couplings
    * - at least two leptons within |eta|<2.7
    * - pt1 > 80 GeV
    */


   if(m_fourMomLep.size()!=2 ){
      cout << "This is not WW case, number of letpons is " << m_fourMomLep.size() << endl;

      if(m_fourMomLep.size()==0) {
         cout << "This is a strange event with no leptons. Should not happen often" << endl;
         for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
            cout << m_argStrEvent.at(j) << endl;
         }

         return 0;
      } else exit(1);
   }


   // filter on eta
   PtMap leptons;
   BOOST_FOREACH(TLorentzVector &l, m_fourMomLep) {
      leptons[l.Pt()]=l;

//      if( fabs(l.Eta()) < 2.7 ) leptons[l.Pt()]=l;
//      if( fabs(l.Eta()) < 5 ) leptons[l.Pt()]=l;

   }


   // multiplicity cut
   if(leptons.size()<2) return 0;


   // pt cuts
   PtMap::iterator firstLep = leptons.begin();
   PtMap::iterator secondLep = leptons.begin(); secondLep++;


   // leading pt
   bool pass = ( firstLep-> second.Pt() > 40); // in GEV

   return pass;

}

// ==========================================================================================================================================

bool LheFilt::FilterEvent_ZZ_2mZ_pt15() {
   /* 
    * Apply pt filter on individual Z, ptZ1, PtZ2 > 15GeV in ZZ events
    */


   if(m_fourMomZ.size()!=2 ){
      cout << "This is not ZZ case, number of Zs is " << m_fourMomZ.size() << endl;


      if(m_fourMomLep.size()==0) {
         cout << "This is a strange event with no leptons. Should not happen often" << endl;
         for (UInt_t j=0; j<m_argStrEvent.size(); j++) {
            cout << m_argStrEvent.at(j) << endl;
         }

         return 0;
      } else exit(1);
   }


   // filter on pt
   PtMap Z;
   BOOST_FOREACH(TLorentzVector &l, m_fourMomZ)  Z[l.Pt()]=l; 


   // pt cuts
   //PtMap::iterator firstZ   = Z.begin();
   PtMap::iterator secondZ  = Z.begin(); secondZ++;


   // both Z pt
   bool pass = ( secondZ-> second.Pt() > 15); // in GEV

   return pass;

}

bool LheFilt::FilterEvent_Splitter() {
   /* 
    * Dummy filter. It is used only to split files 
    */

   return true;

}
// ==========================================================================================================================================

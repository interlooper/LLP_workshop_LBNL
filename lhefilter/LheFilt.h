#ifndef _LHEFILT_H_
#define _LHEFILT_H_


#include "TLorentzVector.h"

#include <vector>
#include <fstream>
#include <string>
#include <sstream>

class LheFilt {

   public :

      bool(LheFilt::*testFunction)();  // test
      std::vector< bool(LheFilt::*)() > m_filters; // vector of pointers to a function
      std::vector<std::string> m_fileList;
      std::vector<std::string> m_argStrEvent;
      std::ifstream  m_inputFile;  
      std::ofstream  m_outputFile;  
      std::string    m_inputFileName;
      std::string    m_currentOutputFileName;
      std::string    m_outputNamePattern;
      std::stringstream m_header;
      std::stringstream m_tail;
      int m_outputFileIdx;
      int m_nevents_split;
      bool m_header_inited;


      std::vector<Double_t> m_xsecs;
      std::vector<Double_t> m_errxsecs;
      Double_t  m_xsec;
      Double_t  m_errxsec;

      Int_t     m_nevread;
      Int_t     m_nevkept;
      Double_t  m_xwgtup;
      Int_t     m_npar;
      Int_t     m_nlep;
      Int_t     m_nZ;

      int debug;
      double m_eff;

//       Int_t     m_pdglep[4];
      std::vector<int> m_pdglep;
      Int_t     m_pdg9;
      std::vector<TLorentzVector> m_fourMomZ;
      std::vector<TLorentzVector> m_fourMomWp;
      std::vector<TLorentzVector> m_fourMomWm;
      std::vector<TLorentzVector> m_fourMomLep;
      TLorentzVector m_fourMom9;


      //  methods:

      LheFilt();
      inline void Set_Input(std::string x) { m_inputFileName = x;  } 
      inline void Set_OutputPattern(std::string x) { m_outputNamePattern = x;  } 
      inline void Set_NeventSplit(int x) { m_nevents_split= x;  } 
      void Print_Configuration();
      void ConstructLheHeader();
      void Set_Filters(std::vector<std::string>  filters);
      bool ExecuteFilters();
      void WriteEvent();
      void FinalizeFile(bool opennewfile);
      void OpenOutputFile();
      void FindInputFiles();
      void FileLoop();
      void FilterEvent();
      void ProcessXsecs();
      void ReadLheHeader(UInt_t iFile);
      bool ReadLheEvent();
//       bool ReadLheEvent_WZ();
      void FilterEvent_WZ();
      bool FilterEvent_WW_pt80();
      bool FilterEvent_WW_pt40();
      bool FilterEvent_Splitter();
      bool FilterEvent_ZZ_pt13pt5();
      bool FilterEvent_ZZ_pt5pt5();
      bool FilterEvent_ZZ_2mZ_pt15();
};

#endif
